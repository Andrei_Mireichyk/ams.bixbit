<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//Auth
Route::post('/auth/registration', 'Api\AuthController@registration');
Route::post('/auth/login', 'Api\AuthController@logIn');
Route::post('/auth/logout', 'Api\AuthController@logOut');

//Получение данных
Route::post('/ams/store', 'Api\AmsController@store');


Route::middleware('jwt.verify')->group(function () {
    Route::get('/ams', 'Api\AmsController@index');


    Route::get('/ams/server_tree', 'Api\AmsController@serverTree');
    Route::get('/ams/errors', 'Api\AmsController@errors');
    Route::post('/ams/clear_errors', 'Api\AmsController@clearErrors');
    Route::post('/ams/cluster', 'Api\AmsController@cluster');
    Route::post('/ams/rack', 'Api\AmsController@rack');
    Route::post('/ams/cell', 'Api\AmsController@cell');

    Route::post('/states/cluster',      'Api\StatesController@cluster');
    Route::post('/states/rack',         'Api\StatesController@rack');
    Route::post('/states/cell',         'Api\StatesController@cell');
    Route::post('/states/device',       'Api\StatesController@device');
    Route::post('/states/hashboard',    'Api\StatesController@hashboard');

    Route::get('/profile', 'Api\ProfileController@index');
    Route::post('/profile/change_password', 'Api\ProfileController@changePassword');
    Route::post('/profile/change_settings', 'Api\ProfileController@changeSettings');
    Route::post('/profile/change_telegram', 'Api\ProfileController@changeTelegram');

    Route::get('/invitation/index', 'Api\InvitationController@index');
    Route::post('/invitation/store', 'Api\InvitationController@store');



});

