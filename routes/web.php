<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/telegram/test', 'WebHook\TelegramController@test');
Route::get('/telegram/set_webhook', 'WebHook\TelegramController@setWebHook');
Route::get('/telegram/remove_webhook', 'WebHook\TelegramController@removeWebHook');
Route::post('/telegram/webhook', 'WebHook\TelegramController@webhook');
Route::get('/telegram/latest_update', 'WebHook\TelegramController@latestUpdate');

Route::get('/{all}', 'PageController@index')->where('all', '.*');
