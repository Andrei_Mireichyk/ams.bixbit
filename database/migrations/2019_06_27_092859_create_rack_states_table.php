<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRackStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rack_states', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->integer('rack_id')->comment('Температура теплоносителя');
            $table->integer('cooler_t')->default(0)->comment('Температура теплоносителя');
            $table->integer('chip_t_average')->default(0)->comment('Средняя температура чипа');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rack_states');
    }
}
