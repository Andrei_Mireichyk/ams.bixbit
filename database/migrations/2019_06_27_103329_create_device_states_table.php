<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateDeviceStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_states', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->integer('device_id');
            $table->string('type');

            $table->ipAddress('ip_address')->default('0.0.0.0')->comment('IP адрес устройства');

            $table->string('version')->comment('Версия устройства');

            $table->bigInteger('hashrate')->default(0)->comment('Хэшрейт устройства');
            $table->string('description')->comment('Описание устройства');

            $table->string('status')->default('offline')->comment('Состаяние устройства');

            $table->integer('power_state')->default(0)->comment('Состаяние питания');
            $table->integer('network_state')->default(0)->comment('Состаяние сетевого подключения');
            $table->integer('health_state')->default(0)->comment('Состаяние устройства');

            $table->integer('board_t')->default(0)->comment('Температура платы');
            $table->integer('chip_t')->default(0)->comment('Температура чипа');

            $table->integer('up_time_sec')->default(0);
            $table->timestamp('update_date')->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device_states');
    }
}
