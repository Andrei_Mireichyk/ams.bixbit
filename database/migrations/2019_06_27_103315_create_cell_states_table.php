<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCellStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cell_states', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('cell_id')->comment('Температура теплоносителя');
            $table->string('type')->nullable()->comment('Тип устройств в ячейке');
            $table->bigInteger('hashrate')->default(0)->comment('Хэшрейт оборудования в ячейке');
            $table->integer('cooler_t')->default(0)->comment('Температура теплоносителя');
            $table->integer('chip_t_average')->default(0)->comment('Средняя температура чипа');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cell_states');
    }
}
