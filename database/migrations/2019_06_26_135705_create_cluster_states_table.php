<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClusterStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cluster_states', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->integer('cluster_id');

            //Охолодитель
            $table->decimal('cooler_t_common', 5,2)->default(0)->comment('Температура общая');
            $table->decimal('cooler_t_entry', 5,2)->default(0)->comment('Температура подачи');
            $table->decimal('cooler_t_out', 5,2)->default(0)->comment('Температура обратки');
            $table->decimal('cooler_pressure', 5,2)->default(0)->comment('Давление в системе');

            $table->decimal('fan_speed', 11,2)->default(0)->comment('Частота вращения вентилятора');

            //Электропотребление (текущий момент) cpc_ - prefix
            $table->decimal('cpc_common', 11,2)->default(0)->comment('текущий момент Общее потребление');
            $table->decimal('cpc_computing_equipment', 11,2)->default(0)->comment('текущий момент ычислительное оборудование');
            $table->decimal('cpc_cooling_equipment', 11,2)->default(0)->comment('текущий момент Охладительное оборудование');

            //Электропотребление (накопленное) apc_ - prefix
            $table->decimal('apc_common', 11,2)->default(0)->comment('накопленное Общее потребление');
            $table->decimal('apc_computing_equipment', 11,2)->default(0)->comment('накопленное Вычислительное оборудование');
            $table->decimal('apc_cooling_equipment', 11,2)->default(0)->comment('накопленное Охладительное оборудование');

            //сила тока
            $table->decimal('amperage_ph1', 11,2)->default(0)->comment('Ток фаза 1');
            $table->decimal('amperage_ph2', 11,2)->default(0)->comment('Ток фаза 2');
            $table->decimal('amperage_ph3', 11,2)->default(0)->comment('Ток фаза 3');

            //Напряжение
            $table->decimal('voltage_ph1', 11,2)->default(0)->comment('Напряжение фаза 1');
            $table->decimal('voltage_ph2', 11,2)->default(0)->comment('Напряжение фаза 2');
            $table->decimal('voltage_ph3', 11,2)->default(0)->comment('Напряжение фаза 3');

            //Остальное
            $table->decimal('pue', 11,2)->default(0)->comment('calculated pue (cluster summary)');
            $table->decimal('power_factor')->default(0)->comment('power factor (cos φ');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cluster_states');
    }
}
