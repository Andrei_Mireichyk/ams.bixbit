<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('board_states', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('hash_board_id');
            $table->string('type');
            $table->bigInteger('hashrate')->comment('Хэшрейт устройства');
            $table->integer('board_t')->default(0);
            $table->integer('chip_t')->default(0);
            $table->integer('health_state')->default(0)->comment('Состаяние устройства');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('board_states');
    }
}
