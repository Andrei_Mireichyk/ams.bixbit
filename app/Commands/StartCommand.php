<?php

namespace App\Commands;

use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

class StartCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = "start";

    /**
     * @var string Command Description
     */
    protected $description = "Start Command to get you started";

    /**
     * @inheritdoc
     */
    public function handle($arguments)
    {

        $update  = $this->getUpdate();
        // This will update the chat status to typing...
        $this->replyWithChatAction(['action' => Actions::TYPING]);

        $this->replyWithMessage(['text' => 'Hello '.$update['message']['from']['first_name']]);
        $this->replyWithMessage(['text' => 'Enter the identifier in the notification settings to receive information in this chat.']);
        $this->replyWithMessage(['text' => 'Identifier  - '.$update['message']['chat']['id']]);

    }
}