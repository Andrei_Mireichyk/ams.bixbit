<?php

namespace App\Http\Controllers\WebHook;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Telegram\Bot\Laravel\Facades\Telegram;

class TelegramController extends Controller
{
    public function webhook()
    {
        $update = Telegram::commandsHandler(true);
        echo "pre";
        var_dump($update);
        Storage::disk('local')->put('bot_responses.txt', serialize($update));
        return 'ok';
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function setWebHook()
    {
        try {
            Telegram::setWebhook(['url' => 'https://ams.bixbit.io/telegram/webhook']);
            return response()->json(['message' => 'Webhook installed']);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'errors' => ['uri' => [$e->getMessage()]]], 422);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeWebHook()
    {
        try {
            Telegram::removeWebhook();
            return response()->json(['message' => 'Webhook deleted.']);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }


    /**
     * @return string
     */
    public function latestUpdate()
    {
        try {
            $string = Storage::disk('local')->get('/bot_responses.txt');
            dd(unserialize($string));
        } catch (\Exception $e) {
            return $e->getMessage();
        }

    }

    /**
     * @return mixed
     */
    public function test()
    {
        return Telegram::getMe();
    }
}
