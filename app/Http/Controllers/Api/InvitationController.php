<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Invitation\InvitationRequest;
use App\Mail\Invitation\RegistrationCode;
use App\Model\Invitation;
use Illuminate\Support\Facades\Mail;

class InvitationController extends Controller
{

    public function index()
    {
        //todo is admin
        $invitations = Invitation::with(['user'])->get();
        return response()->json($invitations);
    }

    public function store(InvitationRequest $request)
    {
        //todo is admin
        $invitation = Invitation::create($request->only([
            'code', 'opc_id'
        ]));

        Mail::to($request->get('email'))->send(new RegistrationCode($invitation));

        return response()->json([
            'message' => __('invitation.created', ['email' => $request->get('email')])
        ]);
    }
}
