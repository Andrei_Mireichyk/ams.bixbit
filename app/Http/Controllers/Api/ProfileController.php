<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Profile\ChangePasswordRequest;
use App\Http\Requests\Profile\ChangeSettingsRequest;
use App\Notifications\ChangeTelegram;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Telegram\Bot\Laravel\Facades\Telegram;

class ProfileController extends Controller
{
    public function index(Request $request)
    {
        return response()->json($request->user());
    }

    public function changeSettings(ChangeSettingsRequest $request)
    {
        $user = $request->user();
        $user->name = $request->get('name');
        $user->save();

        return response()->json(['message' => __('profile.change_settings_success')]);
    }


    /**
     * @param ChangePasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(ChangePasswordRequest $request)
    {

        $user = $request->user();

        if (!Hash::check($request->get('old_password'), $user->password)) {

            return response()->json([
                'message' => __('profile.old_password_not_math'),
                'errors' => ['old_password' => [__('profile.old_password_not_math')]]
            ], 422);

        }

        $user->password = bcrypt($request->get('password'));
        $user->save();

        return response()->json(['message' => __('profile.change_password_success')]);

    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeTelegram(Request $request)
    {

        $user = $request->user();

        $user->setAttribute('telegram_chat_id', $request->get('telegram_chat_id'));

        $user->save();

        $user->notify(new ChangeTelegram());

        return response()->json(['message' => __('profile.change_telegram_success')]);

    }
}
