<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\CellStateResource;
use App\Http\Resources\ClusterStateResource;
use App\Http\Resources\DeviceStateResource;
use App\Http\Resources\HashboardStateResource;
use App\Http\Resources\RackStateResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StatesController extends Controller
{
    /**
     * @param Request $request
     * @return ClusterStateResource
     */
    public function cluster(Request $request)
    {
        $user = $request->user();

        $index = $user->getOpcId() . '/' . $request->get('index');

        $cluster = $user->clusters()
            ->where('cluster_index', $index)
            ->firstOrFail();

        $states = $cluster->states()
            ->betweenDate($request->only(['start', 'end']))
            ->groupHour()
            ->get();

        $hashrates = $cluster->hashrates()
            ->betweenDate($request->only(['start', 'end']))
            ->groupHour()
            ->get()
            ->groupBy('type');

        return new ClusterStateResource(
            (object)[
                'states' => $states,
                'hashrates' => $hashrates,
                'min_date' => $cluster->states()->min('created_at')
            ]);
    }

    /**
     * @param Request $request
     * @return RackStateResource
     */
    public function rack(Request $request)
    {
        $user = $request->user();

        $index = $user->getOpcId() . '/' . $request->get('index');

        $rack = $user->racks()
            ->where('rack_index', $index)
            ->firstOrFail();

        $stats = $rack->states()
            ->betweenDate($request->only(['start', 'end']))
            ->groupHour()
            ->get();

        $hashrates = $rack->hashrates()
            ->betweenDate($request->only(['start', 'end']))
            ->groupHour()->get()
            ->groupBy('type');

        return new RackStateResource(
            (object)[
                'stats'=> $stats,
                'hashrates'=> $hashrates,
                'min_date'=>  $rack->states()->min('created_at')
            ]
        );
    }

    /**
     * @param Request $request
     * @return CellStateResource
     */
    public function cell(Request $request)
    {
        $user = $request->user();

        $index = $user->getOpcId() . '/' . $request->get('index');

        $cell = $user->cells()->where('cell_index', $index)->firstOrFail();

        $states = $cell->states()->betweenDate($request->only(['start', 'end']))->groupHour()->get();

        return new CellStateResource(
            (object)[
                'states'=>$states,
                'min_date'=>$cell->states()->min('created_at')
            ]
        );
    }

    /**
     * @param Request $request
     * @return DeviceStateResource
     */
    public function device(Request $request)
    {
        $user = $request->user();

        $index = $user->getOpcid() . '/' . $request->get('index');

        $device = $user->devices()
            ->where('device_index', $index)
            ->firstOrFail();

        $states = $device->states()
            ->betweenDate($request->only('start', 'end'))
            ->groupHour()
            ->get();

        return new DeviceStateResource(
            (object)[
                'states' => $states,
                'min_date' => $device->states()->min('created_at')
            ]
        );
    }

    /**
     * @param Request $request
     * @return HashboardStateResource
     */
    public function hashboard(Request $request)
    {
        $user = $request->user();

        $index = $user->getOpcId() . '/' . $request->get('index');

        $hashboard = $user->hashboards()
            ->where('hashboard_index', $index)
            ->firstOrFail();

        $states = $hashboard->states()
            ->betweenDate($request->only('start', 'end'))
            ->groupHour()
            ->get();

        return new HashboardStateResource(
            (object)[
                'states' => $states,
                'min_date' => $hashboard->states()->min('created_at')
            ]
        );
    }
}
