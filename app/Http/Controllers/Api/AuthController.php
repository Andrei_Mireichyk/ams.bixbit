<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Auth\LogInRequest;
use App\Http\Requests\Auth\RegistrationRequest;
use App\Http\Controllers\Controller;
use App\Model\Invitation;
use App\User;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    /**
     * @param LogInRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logIn(LogInRequest $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['message' => __('auth.failed') ,'errors'=>['password'=>[__('auth.failed')]]], 422);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        return response()->json(['token' => $token, 'user' => Auth::user()]);
    }



    /**
     * @param RegistrationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function registration(RegistrationRequest $request)
    {


        $invite = Invitation::where('code', $request->get('code'))->first();

        if (!is_null($invite->user_id)) {
            return response()->json(['message' => __('invitation.activated'), 'errors' => ['code' => [__('invitation.activated')]]], 422);
        }

        $credentials = $request->only(['name', 'email', 'password']);

        $credentials['password'] = bcrypt($credentials['password']);
        $user = User::create($credentials);

        $invite->user_id = $user->id;
        $invite->save();

        return response()->json(['message' => __('auth.created'), 'user' => $user], 201);
    }
}
