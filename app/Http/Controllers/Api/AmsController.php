<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Cell;
use App\Model\Cluster;
use App\Model\Device;
use App\Model\HardwareError;
use App\Model\HashBoard;
use App\Model\Invitation;
use App\Model\Rack;
use App\Notifications\HardwareMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;


class AmsController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function serverTree(Request $request)
    {
        $user = $request->user();

        $tree = $user
            ->clusters()
            ->with([
                'racks',
                'racks.cells',
                'racks.cells',
                'racks.cells.devices',
                'racks.cells.devices.boards'
            ])
            ->get();

        return response()->json($tree);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cluster(Request $request)
    {
        $user = $request->user();

        $index = $user->getOpcId() . '/' . $request->get('index');

        $cluster = $user
            ->clusters()
            ->where('cluster_index', $index)
            ->firstOrFail();

        return response()->json($cluster);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function rack(Request $request)
    {
        $user = $request->user();

        $index = $user->getOpcId() . '/' . $request->get('index');

        $rack = $user
            ->racks()
            ->where('rack_index', $index)
            ->firstOrFail();

        return response()->json($rack);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cell(Request $request)
    {
        $user = $request->user();

        $index = $user->getOpcId() . '/' . $request->get('index');

        $cell = $user
            ->cells()
            ->where('cell_index', $index)
            ->with(['devices', 'devices.boards'])
            ->firstOrFail();

        return response()->json($cell);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function errors(Request $request)
    {
        $user = $request->user();

        return response()->json($user->errors->groupBy('device_index'));
    }


    public function clearErrors(Request $request){

        $user = $request->user();

        HardwareError::where('opc_id', $user->getOpcId())->delete();

        return response()->json(['message'=>'Errors removed successfully']);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $xml = simplexml_load_string($request->get('XML'));

        if (!$xml) {
            return response()->json(['message' => 'Bad data'], 422);
        }

        $data = json_encode($xml);
        $data = strtolower($data);

        $data = str_replace('@attributes', 'attributes', $data);
        $data = str_replace('@atributes', 'attributes', $data); //Костыль для неграмотных как я :)
        $data = str_replace(['var_', 'var_env_', 'env_'], '', $data);

        $opc_id = 1;

        $server = json_decode($data);


        //TODO РЕФАКТОРИНГ фасттеория - изменения, уведомления, мониторинг;
        foreach ($server->clusters as $cl_index => $cl) {

            $rack_hashrates = collect();

            $cluster_index = "$opc_id/$cl_index";


            //Обновляем или добавляем кластер
            $cluster = Cluster::updateOrCreate(
                [
                    'cluster_index' => $cluster_index
                ],
                [
                    'opc_id' => $opc_id,
                    'title' => 'Cluster - ' . str_replace('cl_', '', $cl_index),
                ]
            );

            //Добавление информации о кластере
            $cluster_state = $cluster->addState($cl->env);

            $cluster->syncErrors();

            if (!isset($cl->racks)) continue;

            foreach ($cl->racks as $r_index => $r) {

                $cells_hashrates = collect();

                $rack_index = "$opc_id/$cl_index/$r_index";

                $rack = Rack::updateOrCreate(
                    [
                        'rack_index' => $rack_index
                    ],
                    [
                        'opc_id' => $opc_id,
                        'cluster_index' => $cluster_index,
                        'title' => 'Rack - ' . str_replace('r_', '', $r_index),
                    ]
                );

                $rack->addState($r->vars);
                $rack->syncErrors();

                if (!isset($r->cells)) continue;

                foreach ($r->cells as $c_index => $c) {

                    $devices_hashes = collect();

                    $cell_index = "$opc_id/$cl_index/$r_index/$c_index";

                    $cell = Cell::updateOrCreate(
                        [
                            'cell_index' => $cell_index
                        ],
                        [
                            'opc_id' => $opc_id,
                            'rack_index' => $rack_index,
                            'title' => 'Cell - ' . str_replace('c_', '', $c_index),
                        ]
                    );

                    $cell_state = $cell->addState($c->vars);

                    $cell->syncErrors();

                    if (!isset($c->devices)) continue;

                    foreach ($c->devices as $d_index => $d) {

                        $boards_hashes = collect();

                        $device_index = "$opc_id/$cl_index/$r_index/$c_index/$d_index";

                        $device = Device::updateOrCreate(
                            [
                                'device_index' => $device_index
                            ],
                            [
                                'opc_id' => $opc_id,
                                'cell_index' => $cell_index,
                                'title' => $d->attributes->name,
                            ]);


                        $device_state = $device->addState($d->vars);
                        $device->syncErrors();

                        if (!isset($d->boards)) continue;

                        foreach ($d->boards as $b_index => $b) {

                            $board_index = "$opc_id/$cl_index/$r_index/$c_index/$d_index/$b_index";

                            $board = HashBoard::updateOrCreate(
                                [
                                    'hashboard_index' => $board_index
                                ],
                                [
                                    'opc_id' => $opc_id,
                                    'device_index' => $device_index,
                                    'title' => 'Board - ' . str_replace('b_', '', $b_index)
                                ]
                            );

                            $board_state = $board->addState($b->vars);
                            $board->syncErrors();
                            $boards_hashes->push($board_state->getOriginal('hashrate'));
                        }

                        $device_state->hashrate = $boards_hashes->sum();

                        $device_state->save();

                        $devices_hashes->push($device_state->getOriginal('hashrate'));
                    }

                    $cell_state->hashrate = $devices_hashes->sum();


                    if (!empty($device_state)) {
                        $cell_state->type = $device_state->type;
                    }

                    $cell_state->save();

                    $cells_hashrates->push(['type' => $cell_state->type, 'hashrate' => $cell_state->getOriginal('hashrate')]);
                }

                $grouped = [
                    ['type' => 'asic', 'hashrate' => $cells_hashrates->where('type', 'asic')->sum('hashrate')],
                    ['type' => 'gpu', 'hashrate' => $cells_hashrates->where('type', 'gpu')->sum('hashrate')],
                    ['type' => 'fpga', 'hashrate' => $cells_hashrates->where('type', 'fpga')->sum('hashrate')],
                ];

                $rack->hashrates()->createMany($grouped);

                $rack_hashrates->push($grouped);

            }

            $rack_hashrates = $rack_hashrates->collapse();

            $cluster->hashrates()->createMany(
                [
                    ['type' => 'asic', 'hashrate' => $rack_hashrates->where('type', 'asic')->sum('hashrate')],
                    ['type' => 'gpu', 'hashrate' => $rack_hashrates->where('type', 'gpu')->sum('hashrate')],
                    ['type' => 'fpga', 'hashrate' => $rack_hashrates->where('type', 'fpga')->sum('hashrate')],
                ]
            );
        }


        $users = Invitation::with('user')
            ->where('opc_id', $opc_id)
            ->whereNotNull('user_id')
            ->get()
            ->pluck('user');

        $errors = HardwareError::where('opc_id', $opc_id)->where('send_telegram', 0)->get()->groupBy('device_index');

        if (!$errors->isEmpty()) {
            Notification::send($users, new HardwareMessage($errors));
        }

        HardwareError::where('opc_id', $opc_id)->where('send_telegram', 0)->update(['send_telegram' => 1]);

        return response()->json(['message' => 'Data recorded'], 200);

    }
}
