<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property mixed stats
 * @property mixed hashrates
 */
class RackStateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'label' => $this->stats->pluck('date'),
            'cooler_t' => $this->stats->pluck('cooler_t'),
            'chip_t_average' => $this->stats->pluck('chip_t_average'),

            'hashrate' => [
                'asic' => $this->hashrates->has('asic') ? $this->hashrates['asic']->pluck('hashrate') : [],
                'gpu' => $this->hashrates->has('gpu') ? $this->hashrates['gpu']->pluck('hashrate') : [],
                'fpga' => $this->hashrates->has('fpga') ? $this->hashrates['fpga']->pluck('hashrate') : []
            ],


            'hashrate_unit' => [
                'asic' => $this->hashrates->has('asic') ? $this->hashrates['asic']->last()->hashrate_unit : '',
                'gpu' => $this->hashrates->has('gpu') ? $this->hashrates['gpu']->last()->hashrate_unit : '',
                'fpga' => $this->hashrates->has('fpga') ? $this->hashrates['fpga']->last()->hashrate_unit : ''
            ],

            'min_date' => $this->min_date
        ];
    }
}
