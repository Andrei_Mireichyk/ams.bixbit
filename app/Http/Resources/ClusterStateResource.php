<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property mixed states
 * @property mixed hashrates
 * @property mixed min_date
 */
class ClusterStateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'label' => $this->states->pluck('date'),
            'cooler_t_common' => $this->states->pluck('cooler_t_common'),
            'cooler_pressure' => $this->states->pluck('cooler_pressure'),
            'fan_speed' => $this->states->pluck('fan_speed'),
            'cpc_common' => $this->states->pluck('cpc_common'),
            'cpc_computing_equipment' => $this->states->pluck('cpc_computing_equipment'),
            'cpc_cooling_equipment' => $this->states->pluck('cpc_cooling_equipment'),
            'apc_common' => $this->states->pluck('apc_common'),
            'apc_computing_equipment' => $this->states->pluck('apc_computing_equipment'),
            'apc_cooling_equipment' => $this->states->pluck('apc_cooling_equipment'),

            'hashrate' => [
                'asic' => $this->hashrates->has('asic') ? $this->hashrates['asic']->pluck('hashrate') : [],
                'gpu' => $this->hashrates->has('gpu') ? $this->hashrates['gpu']->pluck('hashrate') : [],
                'fpga' => $this->hashrates->has('fpga') ? $this->hashrates['fpga']->pluck('hashrate') : []
            ],

            'hashrate_unit' => [
                'asic' => $this->hashrates->has('asic') ? $this->hashrates['asic']->last()->hashrate_unit : '',
                'gpu' => $this->hashrates->has('gpu') ? $this->hashrates['gpu']->last()->hashrate_unit : '',
                'fpga' => $this->hashrates->has('fpga') ? $this->hashrates['fpga']->last()->hashrate_unit : ''
            ],

            'min_date' => $this->min_date
        ];
    }
}
