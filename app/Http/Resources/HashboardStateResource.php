<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property mixed states
 * @property mixed min_date
 */
class HashboardStateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $hashrate_unit = is_null($this->states->last()) ? 'h' : $this->states->last()->hashrate_unit;

        return [
            'label' => $this->states->pluck('date'),
            'board_t' => $this->states->pluck('board_t'),
            'chip_t' => $this->states->pluck('chip_t'),
            'hashrate' => $this->states->pluck('hashrate'),
            'hashrate_unit' => $hashrate_unit,
            'min_date' => $this->min_date
        ];
    }
}
