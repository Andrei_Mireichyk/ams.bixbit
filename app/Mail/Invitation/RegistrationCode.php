<?php

namespace App\Mail\Invitation;

use App\Model\Invitation;
use http\Env\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * @property Invitation invitation
 * @property mixed code
 */
class RegistrationCode extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @param Invitation $invitation
     */
    public function __construct(Invitation $invitation)
    {
        $this->code = $invitation->code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('vendor.mail.invitation_code')->with([
            'code'=>$this->code,
            'url'=>url('/registration/'.$this->code)
        ]);
    }
}
