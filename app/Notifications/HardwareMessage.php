<?php

namespace App\Notifications;

use App\Model\HardwareError;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\URL;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

/**
 *
 * @property HardwareError errors
 */
class HardwareMessage extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @param HardwareError $errors
     */
    public function __construct($errors)
    {

        $this->errors = $errors;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return !is_null($notifiable->telegram_chat_id) ? [TelegramChannel::class] : [];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->view('vendor.mail.error_message', [
                'text' => $this->error->message
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    /**
     * @param $notifiable
     * @return TelegramMessage
     */
    public function toTelegram($notifiable)
    {

        $content = '*Problems found:*' . PHP_EOL;

        foreach ($this->errors as $index => $group) {
            $content .= '*' . $index . '*' . PHP_EOL;
            foreach ($group as $error) {
                $content .= $error->message . PHP_EOL;
            }
        }

        return TelegramMessage::create()
            ->to($notifiable->telegram_chat_id)
            ->content($content)
            ->button('Check', URL::to('/'));

    }
}
