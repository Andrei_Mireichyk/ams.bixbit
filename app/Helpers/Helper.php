<?php
/**
 * Created by PhpStorm.
 * User: 3
 * Date: 08.07.2019
 * Time: 12:10
 */

namespace App\Helpers;


class Helper
{

    /**
     * Convert power to hashes/sec
     * @param object $hash
     * @return object
     */
    public static function convertToHs(object $hash): object
    {

        switch ($hash->unit) {
            case 'h':
            case 'sol':
                $pow = 1;
                break;

            case 'kh':
                $pow = 2;
                break;

            case 'mh':
                $pow = 3;
                break;

            case 'gh':
                $pow = 4;
                break;

            case 'th':
                $pow = 5;
                break;

            case 'ph':
                $pow = 6;
                break;

            case 'eh':
                $pow = 7;
                break;

            case 'zh':
                $pow = 8;
                break;

            default :
                $pow = 0;
        }

        $hash->value = $hash->value * pow(1000, $pow);

        return $hash;
    }

    /**
     * Convert hashes/sec to max
     * @param int $value
     * @return object
     */
    public static function convertHs(int $value): object
    {

        $pow = floor(strlen($value) / 3)-1;
        $value = $value / pow(1000, $pow);

        return (object)[
            'value' => number_format($value, 4, '.', ""),
            'unit' => self::convertPow($pow)
        ];
    }

    /**
     * Convert pow to name unit
     * @param int $value
     * @return string
     */
    public static function convertPow(int $value): string
    {

        switch ($value) {
            case 1:
                return 'h';

            case 2:
                return 'kh';

            case 3:
                return 'mh';

            case 4:
                return 'gh';

            case 5:
                return 'th';

            case 6:
                return 'ph';

            case 7:
                return 'eh';

            case 8:
                return 'zh';

            default :
                return 'h';
        }
    }
}