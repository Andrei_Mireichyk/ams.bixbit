<?php

namespace App\Model\Hashrates;

use App\Helpers\Helper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * @property \Carbon\Carbon $created_at
 * @property int $id
 * @property \Carbon\Carbon $updated_at
 * @property  $hashrate
 * @property  $hashrate_unit
 */
class RackHashRate extends Model
{
    protected $guarded = [];

    protected $appends = ['hashrate_unit'];


    /**
     * @param $value
     * @return float
     */
    public function getHashrateAttribute($value): float
    {
        return Helper::convertHs($value)->value;
    }


    /**
     * @return string
     */
    public function getHashrateUnitAttribute(): string
    {
        return Helper::convertHs($this->getOriginal('hashrate'))->unit;
    }


    /**
     * @param $query
     * @param $dates
     * @return mixed
     */
    public function scopeBetweenDate($query, $dates)
    {
        $dates['start'] = Carbon::parse($dates['start'])->setTime(0, 0, 0);

        $dates['end'] = Carbon::parse($dates['end'])->setTime(23, 59, 59);

        return $query->whereBetween('created_at', $dates);
    }


    /**
     * @param $query
     * @return mixed
     */
    public function scopeGroupHour($query)
    {

        return $query
            ->select(
                DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d %H:00") as date'),
                DB::raw('avg(hashrate) AS hashrate'),
                DB::raw('type')
            )
            ->groupBy('date', 'type');
    }
}
