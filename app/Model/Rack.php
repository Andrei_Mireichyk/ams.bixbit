<?php

namespace App\Model;

use App\Model\Hashrates\RackHashRate;
use App\Model\states\RackState;
use Illuminate\Database\Eloquent\Model;


/**
 * @property mixed $cells
 * @property \Carbon\Carbon $created_at
 * @property int $id
 * @property \Carbon\Carbon $updated_at
 * @property mixed $states
 * @property mixed $cluster
 * @property mixed $hashrates
 * @property mixed $index
 * @property mixed rack_index
 * @property \Illuminate\Support\Collection $latest_hashrate
 * @property mixed $latest_state
 * @property \Illuminate\Support\Collection $latest_hashrates
 * @property mixed $errors
 * @property mixed $path
 */
class Rack extends Model
{

    protected $guarded = [];

    protected $appends = ['index', 'path', 'latest_state', 'latest_hashrates'];

    /**
     * @return string
     */
    public function getIndexAttribute()
    {
        $array = explode('/', $this->rack_index);
        return end($array);
    }

    /**
     * @return string
     */
    public function getPathAttribute()
    {
        return preg_replace('/^[0-9]+\//', '', $this->rack_index);
    }

    /**
     * @return Model|null|object|static
     */
    public function getLatestStateAttribute()
    {
        return $this->states()->latest('created_at')->first();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getLatestHashratesAttribute()
    {
        return collect([
            'asic' => $this->hashrates()->where('type', 'asic')->latest('created_at')->first(),
            'gpu' => $this->hashrates()->where('type', 'gpu')->latest('created_at')->first(),
            'fpga' => $this->hashrates()->where('type', 'fpga')->latest('created_at')->first()
        ]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cluster()
    {
        return $this->belongsTo(Cluster::class, 'cluster_index', 'cluster_index');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cells()
    {
        return $this->hasMany(Cell::class, 'rack_index', 'rack_index');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function states()
    {
        return $this->hasMany(RackState::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hashrates()
    {
        return $this->hasMany(RackHashRate::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function errors()
    {
        return $this->hasMany(HardwareError::class, 'device_index', 'rack_index');
    }

    /**
     * @param $telemetry
     * @return Model
     */
    public function addState($telemetry)
    {
        return $this->states()->create([
            'cooler_t' => $telemetry->collant_temperature->attributes->value,
            'chip_t_average' => $telemetry->microchip_temperature_average->attributes->value,
        ]);
    }

    /**
     * @return mixed
     */
    public function syncErrors()
    {
        HardwareError::checkHardware($this, 'cooler_t');
        HardwareError::checkHardware($this, 'chip_t_average');

        return $this->errors;
    }

}
