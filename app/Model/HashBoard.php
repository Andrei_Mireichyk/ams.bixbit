<?php

namespace App\Model;

use App\Model\states\BoardState;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed $device
 * @property mixed $states
 * @property bool title
 * @property \Carbon\Carbon $created_at
 * @property int $id
 * @property \Carbon\Carbon $updated_at
 * @property mixed $index
 * @property mixed $latest_state
 * @property mixed $errors
 * @property null|string|string[] $path
 * @property mixed hashboard_index
 */
class HashBoard extends Model
{
    protected $guarded = [];

    protected $appends = ['index', 'path', 'latest_state'];

    /**
     * @return string
     */
    public function getIndexAttribute(): string
    {
        $array = explode('/', $this->hashboard_index);
        return end($array);
    }

    /**
     * @return string
     */
    public function getPathAttribute()
    {
        return preg_replace('/^[0-9]+\//', '', $this->hashboard_index);
    }

    /**
     * @return Model|null|object|static
     */
    public function getLatestStateAttribute()
    {
        return $this->states()->latest('created_at')->first();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function device()
    {
        return $this->belongsTo(Device::class, 'device_index', 'device_index');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function states()
    {
        return $this->hasMany(BoardState::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function errors()
    {
        return $this->hasMany(HardwareError::class, 'device_index', 'hashboard_index');
    }

    /**
     * @param $telemetry
     * @return Model
     */
    public function addState($telemetry)
    {

        $name = BoardState::resolveName($telemetry);

        $chip_t = BoardState::resolveChipT($telemetry);

        $rate = BoardState::resolveHashRate($telemetry);

        $board_t = isset($telemetry->board_temperature) ? $telemetry->board_temperature->attributes->value : 0;

        return $this->states()->create(
            [
                'type' => '',
                'hashrate' => $rate->value,
                'board_t' => (int)$board_t,
                'chip_t' => (int)$chip_t,
                'health_state' => (int)!!$telemetry->diag_health_ok->attributes->value
            ]
        );
    }

    /**
     * @return mixed
     */
    public function syncErrors()
    {
        HardwareError::checkHardware($this, 'board_t');
        HardwareError::checkHardware($this, 'chip_t');

        return $this->errors;
    }

}
