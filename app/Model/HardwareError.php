<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed message
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $created_at
 * @property int $id
 * @property mixed $time_ago
 */
class HardwareError extends Model
{
    protected $guarded = [];

    protected $appends = ['time_ago'];

    /**
     * @return string
     */
    public function getTimeAgoAttribute(){
        return Carbon::parse($this->created_at)->diffForHumans();
    }
    /**
     * @param $hardware
     * @param $error_code
     * @return mixed
     */
    protected static function add($hardware, $error_code)
    {
        return $hardware->errors()->updateOrcreate(
            [
                'opc_id' => $hardware->opc_id,
                'code' => $error_code
            ],
            [
                'type' => 'error',
                'title' => __('ams_errors.' . $error_code, ['value'=>$hardware->latest_state->$error_code]),
                'message' => __('ams_errors.' . $error_code, ['value'=>$hardware->latest_state->$error_code])
            ]);
    }

    /**
     * @param $hardware
     * @param $error_code
     * @return mixed
     */
    protected static function remove($hardware, $error_code)
    {
          return $hardware->errors()
            ->where('code', $error_code)
            ->where('device_index', $hardware->opc_id . '/' . $hardware->path)
            ->delete();
    }

    /**
     * @param $hardware
     * @param $error_index
     */
    public static function checkHardware($hardware, $error_index)
    {
        $value = $hardware->latest_state->getAttribute($error_index);

        static::$error_index($hardware, $value);
    }

    /**
     * @param $hardware
     * @param $value
     */
    protected static function cooler_t($hardware, $value)
    {
        if ($value <= 0 || $value >= 100)
            static::add($hardware, 'cooler_t');
        else
            static::remove($hardware, 'cooler_t');

    }

    /**
     * @param $hardware
     * @param $value
     */
    protected static function chip_t_average($hardware, $value)
    {
        if ($value <= 0 || $value >= 100)
            static::add($hardware, 'chip_t_average');
        else
            static::remove($hardware, 'chip_t_average');
    }

    /**
     * @param $hardware
     * @param $value
     */
    protected static function board_t($hardware, $value)
    {
        if ($value <= 0 || $value >= 100)
            static::add($hardware, 'board_t');
        else
            static::remove($hardware, 'board_t');
    }

    /**
     * @param $hardware
     * @param $value
     */
    protected static function chip_t($hardware, $value)
    {
        if ($value <= 0 || $value >= 100)
            static::add($hardware, 'chip_t');
        else
            static::remove($hardware, 'chip_t');
    }

    /**
     * @param $hardware
     * @param $value
     */
    protected static function cooler_t_common($hardware, $value)
    {
        if ($value <= 0 || $value >= 100)
            static::add($hardware, 'cooler_t_common');
        else
            static::remove($hardware, 'cooler_t_common');
    }

    /**
     * @param $hardware
     * @param $value
     */
    protected static function cooler_t_entry($hardware, $value)
    {
        if ($value <= 0 || $value >= 100)
            static::add($hardware, 'cooler_t_entry');
        else
            static::remove($hardware, 'cooler_t_entry');
    }

    /**
     * @param $hardware
     * @param $value
     */
    protected static function cooler_t_out($hardware, $value)
    {
        if ($value <= 0 || $value >= 100)
            static::add($hardware, 'cooler_t_out');
        else
            static::remove($hardware, 'cooler_t_out');
    }

    /**
     * @param $hardware
     * @param $value
     */
    protected static function cooler_pressure($hardware, $value)
    {
        if ($value <= 0)
            static::add($hardware, 'cooler_pressure');
        else
            static::remove($hardware, 'cooler_pressure');
    }

    /**
     * @param $hardware
     * @param $value
     */
    protected static function cpc_common($hardware, $value)
    {
        if ($value <= 0)
            static::add($hardware, 'cpc_common');
        else
            static::remove($hardware, 'cpc_common');
    }

    /**
     * @param $hardware
     * @param $value
     */
    protected static function cpc_computing_equipment($hardware, $value)
    {
        if ($value <= 0)
            static::add($hardware, 'cpc_computing_equipment');
        else
            static::remove($hardware, 'cpc_computing_equipment');
    }
    /**
     * @param $hardware
     * @param $value
     */
    protected static function cpc_cooling_equipment($hardware, $value)
    {
        if ($value <= 0)
            static::add($hardware, 'cpc_cooling_equipment');
        else
            static::remove($hardware, 'cpc_cooling_equipment');
    }

    /**
     * @param $hardware
     * @param $value
     */
    protected static function apc_common($hardware, $value)
    {
        if ($value <= 0)
            static::add($hardware, 'apc_common');
        else
            static::remove($hardware, 'apc_common');

    }

    /**
     * @param $hardware
     * @param $value
     */
    protected static function apc_computing_equipment($hardware, $value)
    {
        if ($value <= 0)
            static::add($hardware, 'apc_computing_equipment');
        else
            static::remove($hardware, 'apc_computing_equipment');

    }

    /**
     * @param $hardware
     * @param $value
     */
    protected static function apc_cooling_equipment($hardware, $value)
    {
        if ($value <= 0)
            static::add($hardware, 'apc_cooling_equipment');
        else
            static::remove($hardware, 'apc_cooling_equipment');
    }

}








