<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * @property \Carbon\Carbon $created_at
 * @property int $id
 * @property \Carbon\Carbon $updated_at
 * @property mixed $user
 */
class Ams extends Model
{
    protected $fillable = ['user_id', 'data'];


    public function user()
    {
        return $this->hasOne(User::class);
    }
}
