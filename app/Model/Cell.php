<?php

namespace App\Model;

use App\Model\Hashrates\RackHashRate;
use App\Model\states\CellState;
use Illuminate\Database\Eloquent\Model;

/**
 * @property \Carbon\Carbon $created_at
 * @property int $id
 * @property \Carbon\Carbon $updated_at
 * @property mixed $devices
 * @property mixed $states
 * @property mixed $hashrates
 * @property mixed $index
 * @property mixed $cell_index
 * @property \Illuminate\Database\Eloquent\Builder $latest_state
 * @property mixed $latest_hashrate
 * @property mixed $errors
 * @property null|string|string[] $path
 */
class Cell extends Model
{
    protected $guarded = [];

    protected $appends = ['index', 'path', 'latest_state'];

    /**
     * @return string
     */
    public function getIndexAttribute(): string
    {
        $array = explode('/', $this->cell_index);
        return end($array);
    }

    /**
     * @return string
     */
    public function getPathAttribute()
    {
        return preg_replace('/^[0-9]+\//', '', $this->cell_index);
    }

    /**
     * @return Model|null|object|static
     */
    public function getLatestStateAttribute()
    {
        return $this->states()->latest('created_at')->first();
    }

    /**
     * @return mixed
     */
    public function rack()
    {
        return $this->belongTo(Rack::class, 'rack_index', 'rack_index');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function devices()
    {
        return $this->hasMany(Device::class, 'cell_index', 'cell_index');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function states()
    {
        return $this->hasMany(CellState::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hashrates()
    {
        return $this->hasMany(RackHashRate::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function errors()
    {
        return $this->hasMany(HardwareError::class, 'device_index', 'cell_index');
    }

    /**
     * @param $telemetry
     * @return Model
     */
    public function addState($telemetry)
    {

        return $this->states()->create([
            'cooler_t' => $telemetry->collant_temperature->attributes->value,
            'chip_t_average' => $telemetry->microchip_temperature_average->attributes->value,
        ]);
    }

    /**
     * @return mixed
     */
    public function syncErrors()
    {
        HardwareError::checkHardware($this, 'cooler_t');
        HardwareError::checkHardware($this, 'chip_t_average');

        return $this->errors;
    }

}
