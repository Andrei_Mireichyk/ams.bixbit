<?php

namespace App\Model;

use App\Model\states\DeviceState;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property \Carbon\Carbon $created_at
 * @property int $id
 * @property \Carbon\Carbon $updated_at
 * @property mixed $devices
 * @property mixed $states
 * @property mixed $boards
 * @property mixed $index
 * @property mixed device_index
 * @property mixed $latest_state
 * @property mixed $errors
 * @property null|string|string[] $path
 */
class Device extends Model
{
    protected $guarded = [];

    protected $appends = ['index', 'path','latest_state'];

    /**
     * @return string
     */
    public function getIndexAttribute(): string
    {
        $array = explode('/', $this->device_index);
        return end($array);
    }

    /**
     * @return string
     */
    public function getPathAttribute()
    {
        return preg_replace('/^[0-9]+\//', '', $this->device_index);
    }

    /**
     * @return Model|null|object|static
     */
    public function getLatestStateAttribute()
    {
        return $this->states()->latest('created_at')->first();
    }

    /**
     * @return mixed
     */
    public function cell()
    {
        return $this->belongTo(Cell::class, 'cell_index', 'cell_index');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function boards()
    {
        return $this->hasMany(HashBoard::class, 'device_index', 'device_index');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function states()
    {
        return $this->hasMany(DeviceState::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function errors()
    {
        return $this->hasMany(HardwareError::class, 'device_index', 'device_index');
    }

    /**
     * @param $telemetry
     * @return Model
     */
    public function addState($telemetry)
    {

        $rate = DeviceState::resolveHashRate($telemetry);

        $version = isset($telemetry->miner_version) ? $telemetry->miner_version->attributes->value : '';

        $miner_description = isset($telemetry->miner_description) ? $telemetry->miner_description->attributes->value : '';

        $device_status = isset($telemetry->device_status) ? $telemetry->device_status->attributes->value : '';

        $type = empty($version) ? 'gpu' : 'asic';

        return $this->states()->create(
            [
                'ip_address' => $telemetry->network_adress->attributes->value,
                'version' => $version,
                'type' => $type,
                'hashrate' => $rate->value,
                'description' => $miner_description,
                'status' => $device_status,
                'power_state' => (int)!!$telemetry->env->device_power_state->attributes->value,
                'network_state' => (int)!!$telemetry->device_connection_state->attributes->value,
                'health_state' => (int)!!$telemetry->diag_health_ok->attributes->value,
                'board_t' => 0, //todo нет данных
                'chip_t' => 0, //todo нет данных
                'up_time_sec' => $telemetry->device_uptime_seconds->attributes->value,
                'update_date' => Carbon::createFromTimestamp($telemetry->device_last_update_timestamp->attributes->value),
            ]
        );
    }

    /**
     * @return mixed
     */
    public function syncErrors()
    {
        HardwareError::checkHardware($this, 'board_t');
        HardwareError::checkHardware($this, 'chip_t');

        return $this->errors;
    }
}
