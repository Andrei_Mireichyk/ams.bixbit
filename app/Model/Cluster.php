<?php

namespace App\Model;

use App\Model\Hashrates\ClusterHashRate;
use App\Model\States\ClusterState;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed $racks
 * @property \Carbon\Carbon $created_at
 * @property int $id
 * @property \Carbon\Carbon $updated_at
 * @property mixed $states
 * @property mixed $hashrates
 * @property mixed $index
 * @property \Illuminate\Support\Collection $latest_hashrate
 * @property mixed $latest_state
 * @property \Illuminate\Support\Collection $latest_hashrates
 * @property null|string|string[] $path
 * @property mixed errors
 * @property mixed cluster_index
 */
class Cluster extends Model
{
    protected $guarded = [];

    protected $appends = ['index', 'path', 'latest_state', 'latest_hashrates'];

    /**
     * @return string
     */
    public function getIndexAttribute(): string
    {
        $array = explode('/', $this->cluster_index);
        return end($array);
    }

    /**
     * @return string
     */
    public function getPathAttribute()
    {
        return preg_replace('/^[0-9]+\//', '', $this->cluster_index);
    }

    /**
     * @return Model|null|object|static
     */
    public function getLatestStateAttribute()
    {
        return $this->states()->latest('created_at')->first();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getLatestHashratesAttribute()
    {
        return collect([
            'asic' => $this->hashrates()->where('type', 'asic')->latest('created_at')->first(),
            'gpu' => $this->hashrates()->where('type', 'gpu')->latest('created_at')->first(),
            'fpga' => $this->hashrates()->where('type', 'fpga')->latest('created_at')->first()
        ]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function racks()
    {
        return $this->hasMany(Rack::class, 'cluster_index', 'cluster_index');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function states()
    {
        return $this->hasMany(ClusterState::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hashrates()
    {
        return $this->hasMany(ClusterHashRate::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function errors()
    {
        return $this->hasMany(HardwareError::class, 'device_index', 'cluster_index');
    }

    /**
     * @param $telemetry
     * @return ClusterState|Model
     */
    public function addState($telemetry)
    {
        return $this->states()->create([

            'cooler_t_common' => (int)$telemetry->collant_temperature_cluster->attributes->value,

            'cooler_t_entry' => 0,//Todo нет данных
            'cooler_t_out' => 0,//Todo нет данных

            'cooler_pressure' => (int)$telemetry->collant_pressure->attributes->value,
            'fan_speed' => (int)$telemetry->fan_speeds->fan1->attributes->value,

            'cpc_common' => (int)$telemetry->powerconsumption_realtime->attributes->value,
            'cpc_computing_equipment' => (int)$telemetry->powerconsumption_realtime_computers->attributes->value,
            'cpc_cooling_equipment' => (int)$telemetry->powerconsumption_realtime_coolers->attributes->value,

            'apc_common' => (int)$telemetry->powerconsumption_summary->attributes->value,
            'apc_computing_equipment' => (int)$telemetry->powerconsumption_summary_computers->attributes->value,
            'apc_cooling_equipment' => (int)$telemetry->powerconsumption_summary_coolers->attributes->value,

            'amperage_ph1' => 0,//Todo нет данных
            'amperage_ph2' => 0,//Todo нет данных
            'amperage_ph3' => 0,//Todo нет данных

            'voltage_ph1' => (int)$telemetry->claster_voltages->voltage_cluster_ph1->attributes->value,
            'voltage_ph2' => (int)$telemetry->claster_voltages->voltage_cluster_ph2->attributes->value,
            'voltage_ph3' => (int)$telemetry->claster_voltages->voltage_cluster_ph3->attributes->value,

            'pue' => (int)$telemetry->pue_cluster->attributes->value,
            'power_factor' => (int)$telemetry->powerfactor_cluster->attributes->value,
        ]);
    }

    /**
     * @return mixed
     */
    public function syncErrors()
    {
        HardwareError::checkHardware($this, 'cooler_t_common');
        HardwareError::checkHardware($this, 'cooler_t_entry');
        HardwareError::checkHardware($this, 'cooler_t_out');
        HardwareError::checkHardware($this, 'cooler_pressure');
        HardwareError::checkHardware($this, 'cooler_t_entry');
        HardwareError::checkHardware($this, 'cpc_common');
        HardwareError::checkHardware($this, 'cpc_computing_equipment');
        HardwareError::checkHardware($this, 'cpc_cooling_equipment');
        HardwareError::checkHardware($this, 'apc_common');
        HardwareError::checkHardware($this, 'apc_computing_equipment');
        HardwareError::checkHardware($this, 'apc_cooling_equipment');

        return $this->errors;
    }

}
