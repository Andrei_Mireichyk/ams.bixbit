<?php

namespace App;

use App\Model\Alert;
use App\Model\Ams;
use App\Model\Cell;
use App\Model\Cluster;
use App\Model\Device;
use App\Model\HardwareError;
use App\Model\HardwareNotification;
use App\Model\HashBoard;
use App\Model\Invitation;
use App\Model\Notification;
use App\Model\Rack;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * @property \Carbon\Carbon $created_at
 * @property int $id
 * @property \Carbon\Carbon $updated_at
 * @property mixed $ams
 * @property \Carbon\Carbon $email_verified_at
 * @property mixed $clusters
 * @property mixed $invitation
 * @property mixed $racks
 * @property mixed $cells
 * @property mixed $devices
 * @property mixed $hashboards
 * @property mixed $alerts
 * @property mixed $notifications
 * @property mixed $errors
 */
class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    function ams()
    {
        return $this->belongsToMany(Ams::class, 'invitations', 'user_id', 'opc_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    function clusters()
    {
        return $this->belongsToMany(Cluster::class, 'invitations', 'user_id', 'opc_id', 'id', 'id', 'opc_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    function racks()
    {
        return $this->belongsToMany(Rack::class, 'invitations', 'user_id', 'opc_id', 'id', 'opc_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    function cells()
    {
        return $this->belongsToMany(Cell::class, 'invitations', 'user_id', 'opc_id', 'id', 'opc_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    function devices()
    {
        return $this->belongsToMany(Device::class, 'invitations', 'user_id', 'opc_id', 'id', 'opc_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    function hashboards()
    {
        return $this->belongsToMany(HashBoard::class, 'invitations', 'user_id', 'opc_id', 'id', 'opc_id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    function errors()
    {
        return $this->belongsToMany(HardwareError::class, 'invitations', 'user_id', 'opc_id', 'id', 'opc_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function invitation()
    {
        return $this->hasOne(Invitation::class);
    }

    public function getOpcId()
    {
        return $this->invitation->opc_id;
    }
}
