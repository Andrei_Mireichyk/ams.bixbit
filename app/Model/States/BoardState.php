<?php

namespace App\Model\states;

use App\Helpers\Helper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * @property \Carbon\Carbon $created_at
 * @property int $id
 * @property \Carbon\Carbon $updated_at
 * @property  $hashrate_unit
 * @property  $hashrate
 */
class BoardState extends Model
{
    protected $guarded = [];

    protected $appends = ['hashrate_unit'];

    /**
     * @param $telemetry
     */
    /**
     * @param $telemetry
     * @return object
     */
    public static function resolveHashRate($telemetry): object
    {

        $hash = (object)[
            'unit' => '',
            'value' => 0
        ];

        foreach ($telemetry->hashrate as $key => $value) {

            list(, , $unit, $type) = explode("_", $key);

            if ($type === 'rt') {
                $hash->value = $value->attributes->value;
                $hash->unit = preg_replace('/s$/', '', $unit);
            }
        }


        return Helper::convertToHs($hash);
    }


    /**
     * @param $telemetry
     * @return int
     */
    public static function resolveChipT($telemetry): int
    {

        if (isset($telemetry->board_temperature_chips)) {
            return max(explode('-', $telemetry->board_temperature_chips->attributes->value));
        }

        if (isset($telemetry->board_temperature_chip)) {
            return $telemetry->board_temperature_chip->attributes->value;
        }

        return 0;

    }

    /**
     * @param $telemetry
     * @return bool
     */
    public static function resolveName($telemetry): bool
    {

        if (isset($telemetry->board_temperature)) {
            return 'Asic';
        }

        if (isset($telemetry->board_temperature_chip)) {
            return 'Gpu';
        }

        return 'Asic';
    }

    /**
     * @param $value
     * @return float
     */
    public function getHashrateAttribute($value): float
    {
        return Helper::convertHs($value)->value;
    }


    /**
     * @return string
     */
    public function getHashrateUnitAttribute(): string
    {
        return Helper::convertHs($this->getOriginal('hashrate'))->unit;
    }

    /**
     * @param $query
     * @param $dates
     * @return mixed
     */
    public function scopeBetweenDate($query, $dates)
    {
        $dates['start'] = Carbon::parse($dates['start'])->setTime(0, 0, 0);

        $dates['end'] = Carbon::parse($dates['end'])->setTime(23, 59, 59);

        return $query->whereBetween('created_at', $dates);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeGroupHour($query)
    {
        return $query
            ->select(
                DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d %H:00") as date'),
                DB::raw('avg(hashrate) AS hashrate'),
                DB::raw('FORMAT(avg(board_t),0) AS board_t'),
                DB::raw('FORMAT(avg(chip_t),0) AS chip_t')
            )
            ->groupBy('date');
    }
}