<?php

namespace App\Model\States;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * @property \Carbon\Carbon $created_at
 * @property int $id
 * @property \Carbon\Carbon $updated_at
 * @property mixed $hash_rates
 */
class ClusterState extends Model
{
    protected $guarded = [];


    /**
     * @param $query
     * @param $dates
     * @return mixed
     */
    public function scopeBetweenDate($query, $dates)
    {
        $dates['start'] = Carbon::parse($dates['start'])->setTime(0, 0, 0);

        $dates['end'] = Carbon::parse($dates['end'])->setTime(23, 59, 59);

        return $query->whereBetween('created_at', $dates);
    }


    /**
     * @param $query
     * @return mixed
     */
    public function scopeGroupHour($query)
    {

        return $query
            ->select(
                DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d %H:00") as date'),
                DB::raw('FORMAT(avg(cooler_t_common),0) AS cooler_t_common'),
                DB::raw('FORMAT(avg(cooler_pressure), 2) AS cooler_pressure'),
                DB::raw('FORMAT(avg(fan_speed),0) AS fan_speed'),
                DB::raw('FORMAT(avg(cpc_common),0) AS cpc_common'),
                DB::raw('FORMAT(avg(cpc_computing_equipment),0) AS cpc_computing_equipment'),
                DB::raw('FORMAT(avg(cpc_cooling_equipment),0) AS cpc_cooling_equipment'),
                DB::raw('FORMAT(avg(apc_common),0) AS apc_common'),
                DB::raw('FORMAT(avg(apc_computing_equipment),0) AS apc_computing_equipment'),
                DB::raw('FORMAT(avg(apc_cooling_equipment),0) AS apc_cooling_equipment')
            )
            ->groupBy('date');
    }
}
