@setup
$on = 'production';
$user = "ams_deployer";
$password = "G,VmY7*db{Z&3_`JLfa^";
$timezone = "Europe/Moscow";

$path = "/sites/ams_bixbit_io";
$current = $path . "/current";

$repo = "git@bitbucket.org:Andrei_Mireichyk/ams.bixbit.git";

$branch = "master";

$chmods = [
"storage",
"storage/app",
"storage/framework",
"storage/logs"
];

$date = new datetime("now", new DateTimeZone($timezone));
$release = $path . "/releases/" . $date->format("YmdHis");

echo "#0 - setup section"
@endsetup

@servers([$on => "ams_deployer@37.140.198.98"])

@task("clone", ["on" => $on])

mkdir -p {{$release}}

git clone --depth 1 -b {{$branch}} "{{$repo}}" {{$release}}

echo "#1 - Repository has been cloned"
@endtask

@task("composer", ["on" => $on])
composer self-update

cd {{$release}}

composer install --no-interaction --no-dev --prefer-dist

echo "#2 - Composer dependecies have been installed"
@endtask

@task("artisan", ["on" => $on])
cd {{$release}}

ln -nfs {{$path}}/.env .env;
chgrp -h www-data .env;

php artisan config:clear

php artisan migrate

echo "#3 - Composer dependecies have been installed"
@endtask

@task("chmod", ["on" => $on])
chgrp -R www-data {{$release}};
chmod -R ug+rwx {{$release}};

@foreach ($chmods as $file)
    chmod -R 775 {{$release}}/{{$file}}

    chown -R {{$user}}:www-data {{$release}}/{{$file}}

    echo "Permissions have been set for {{$file}}"
@endforeach

echo "#4 - Permissions has been set"
@endtask

@task("update_symlinks", ["on" => $on])

ln -nfs {{$release}} {{$current}};

chgrp -h www-data {{$current}};

echo "#5 - Symlink has been set"

echo {{$release}} {{$current}};
@endtask


@story("deploy", ["on" => $on])
clone
composer
artisan
chmod
update_symlinks
@endstory