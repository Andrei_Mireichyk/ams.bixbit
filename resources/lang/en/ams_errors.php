<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'cooler_t_common' => 'Problem with Total temperature reading - :value °C',
    'cooler_t_entry' => 'Problem with Cooler temperature entry reading - :value °C',
    'cooler_t_out' => 'Problem with Cooler temperature out reading - :value °C',
    'cooler_pressure' => 'Problem with System pressure reading - :value bar',
    'cpc_common' => 'Problem with Total consumption (current) reading - :value watt',
    'cpc_computing_equipment' => 'Problem with Computing equipment (current) reading - :value watt',
    'cpc_cooling_equipment' => 'Problem with Cooling equipment (current) reading - :value watt',
    'apc_common' => 'Problem with Total consumption (Total consumption) reading - :value watt',
    'apc_computing_equipment' => 'Problem with Computing equipment (Total consumption) reading - :value watt',
    'apc_cooling_equipment' => 'Problem with Cooling equipment (Total consumption) reading- :value watt',
    'cooler_t' => 'Problem with Coolant temperature reading - :value °C',
    'chip_t_average' => 'Problem with Chip average temperature reading - :value °C',
    'board_t' => 'Problem with Board temperature reading - :value °C',
    'chip_t' => 'Problem with Chip temperature reading - :value °C',

];
