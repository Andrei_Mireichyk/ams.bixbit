<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */


    'change_telegram_success' => 'Telegram successfully changed',
    'old_password_not_math' => 'The old password does not match our records',
    'change_password_success' => 'Password successfully changed',
    'change_settings_success' => 'Settings successfully changed'

];
