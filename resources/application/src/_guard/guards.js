function auth ({ next, store }) {
  let auth = store.getters['auth/auth']
  if (!auth) next('/login')
}

function not_auth ({ next, store }) {
  let auth = store.getters['auth/auth']
  if (auth) next('/panel')
}

export default {
  auth,
  not_auth
}
