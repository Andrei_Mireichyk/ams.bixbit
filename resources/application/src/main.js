import './bootstrap.js'
import Vue from 'vue'
import router from './router'
import store from './_store'
import App from './App.vue'

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
