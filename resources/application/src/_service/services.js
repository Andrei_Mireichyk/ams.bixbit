import store from '../_store'

export let errorHandling = function (res) {
  let response = res.response

  let message = response.data.message

  if (response.status === 401) {
    store.dispatch('auth/logOut')
  }

  if (response.status === 422) {
    let errors = response.data.errors

    if (message) this.$message.error(message)

    for (let field in errors) {
      this.$validator.errors.add({ field: field, 'msg': errors[field].shift() })
    }
  } else {
    if (message) this.$message.error('Error - ' + response.status)
  }
}

export let resolveInputState = function (state) {
  return state ? 'error' : 'success'
}

export let getHardware = function (index) {
  let tree = store.getters['tree/tree']

  let [, cluster_index, rack_index, cell_index, device_index, board_index] = index.split('/')

  if (board_index) {
    let cluster = tree.find(item => item.index === cluster_index)
    let rack = cluster.racks.find(item => item.index === rack_index)
    let cell = rack.cells.find(item => item.index === cell_index)
    let device = cell.devices.find(item => item.index === device_index)
    let board = device.boards.find(item => item.index === board_index)

    return {
      href: `/panel/cluster/${cluster.index}/rack/${rack.index}/cell/${cell.index}`,
      breadcrumbs: `${cluster.title}/${rack.title}/${cell.title}/${device.title}/${board.title}`,
      hardware: board
    }
  } else if (device_index) {
    let cluster = tree.find(item => item.index === cluster_index)
    let rack = cluster.racks.find(item => item.index === rack_index)
    let cell = rack.cells.find(item => item.index === cell_index)
    let device = cell.devices.find(item => item.index === device_index)

    return {
      href: `/panel/cluster/${cluster.index}/rack/${rack.index}/cell/${cell.index}`,
      breadcrumbs: `${cluster.title}/${rack.title}/${cell.title}/${device.title}`,
      hardware: device
    }
  } else if (cell_index) {
    let cluster = tree.find(item => item.index === cluster_index)
    let rack = cluster.racks.find(item => item.index === rack_index)
    let cell = rack.cells.find(item => item.index === cell_index)

    return {
      href: `/panel/cluster/${cluster.index}/rack/${rack.index}/cell/${cell.index}`,
      breadcrumbs: `${cluster.title}/${rack.title}/${cell.title}`,
      hardware: cell
    }
  } else if (rack_index) {
    let cluster = tree.find(item => item.index === cluster_index)
    let rack = cluster.racks.find(item => item.index === rack_index)

    return {
      href: `/panel/cluster/${cluster.index}/rack/${rack.index}`,
      breadcrumbs: `${cluster.title}/${rack.title}`,
      hardware: rack
    }
  } else if (cluster_index) {
    let cluster = tree.find(item => item.index === cluster_index)

    return {
      href: `/panel/cluster/${cluster.index}`,
      breadcrumbs: `${cluster.title}`,
      hardware: cluster
    }
  }
}

export let getHardwareErrors = function (hardware) {
  if (!hardware) return []

  let hardwareErrors = store.getters['errors/errors']

  let hardware_index = `${hardware.opc_id}/${hardware.path}`

  let error_group = hardwareErrors[hardware_index]

  let errors = []

  for (let error_index in error_group) {
    let error = error_group[error_index]

    let device_info = this.$getHardware(error.device_index)

    errors.push({
      key: error.id,
      device_info: { title: device_info.breadcrumbs, type: error.type },
      message: error.message,
      time_ago: error.time_ago,
      code: error.code
    })
  }

  return errors
}

export let getHardwareNestedErrors = function (hardware) {
  if (!hardware) return []

  let hardwareErrors = store.getters['errors/errors']
  let errors = []
  for (let hardwareIndex in hardwareErrors) {
    if (hardwareIndex.search(hardware.path) >= 0) {
      let errorGroup = hardwareErrors[hardwareIndex]

      for (let errorIndex in errorGroup) {
        let error = errorGroup[errorIndex]

        let deviceInfo = this.$getHardware(error.device_index)

        errors.push({
          key: error.id,
          device_info: { title: deviceInfo.breadcrumbs, type: error.type },
          message: error.message,
          time_ago: error.time_ago,
          code: error.code
        })
      }
    }
  }

  return errors
}

export default {
  errorHandling,
  resolveInputState,
  getHardware,
  getHardwareErrors,
  getHardwareNestedErrors
}
