import Vue from 'vue'
import Router from 'vue-router'
import store from './_store'
import guards from './_guard/guards'

import Rack from './views/Rack.vue'
import Cell from './views/Cell.vue'
import Cluster from './views/Cluster.vue'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    { path: '/panel', redirect: '/panel/dashboard' },
    { path: '/', redirect: '/panel/dashboard' },

    {
      path: '/login',
      name: 'login',
      component: () => import('./views/Auth/LogIn.vue'),
      meta: {
        guards: ['not_auth']
      }
    },

    {
      path: '/registration/:code?',
      name: 'registration',
      component: () => import('./views/Auth/Registration.vue'),
      meta: {
        guards: ['not_auth']
      }
    },
    {
      path: '/panel',
      name: 'panel',
      component: () => import('./views/Panel/Panel.vue'),

      meta: {
        guards: ['auth']
      },

      async beforeEnter (to, from, next) {
        await store.dispatch('tree/tree')
        store.dispatch('profile/profile')
        store.dispatch('errors/errors')
        next()
      },

      children: [
        {
          path: 'dashboard',
          name: 'dashboard',
          component: () => import('./views/Dashboard/Dashboard.vue')
        },
        {
          path: 'invitations',
          name: 'invitations',
          component: () => import('./views/Invitations.vue')
        },
        {
          path: 'cluster/:cluster_index',
          name: 'cluster',
          component: Cluster
        },

        {
          path: 'cluster/:cluster_index/rack/:rack_index',
          name: 'rack',
          component: Rack
        },
        {
          path: 'cluster/:cluster_index/rack/:rack_index/cell/:cell_index',
          name: 'cell',
          component: Cell
        }
      ]
    }

  ]
})

// Guards protection
router.beforeEach((to, from, next) => {
  const ctx = { to, from, next, router, store }

  to.matched.map(route => {
    if (!route.meta.guards) return false

    route.meta.guards.map(name_guard => {
      if (name_guard in guards) guards[name_guard](ctx)
    })
  })

  next()
})

export default router
