import Axios from 'axios'
import config from '../app.config'
import { errorHandling } from '../_service/services'

const tree = {
  namespaced: true,
  state: {
    tree: {}
  },
  getters: {

    tree (state) {
      return state.tree
    }

  },
  mutations: {

    tree (state, payload) {
      state.tree = payload
    }

  },
  actions: {
    async  tree ({ commit }) {
      await Axios.get(`${config.hostApi}/ams/server_tree`)

        .then(res => {
          commit('tree', res.data)
        })

        .catch(res => {
          errorHandling(res)
        })
    }
  }
}
export default tree
