import axios from 'axios'
import config from '../app.config'
import services from '../_service/services'

const profile = {
  namespaced: true,

  state: {
    profile: {}
  },

  getters: {
    profile (state) {
      return state.profile
    }
  },

  mutations: {
    profile (state, payload) {
      state.profile = payload
    }
  },

  actions: {
    profile ({ state, commit }) {
      axios.get(`${config.hostApi}/profile`)

        .then(res => {
          commit('profile', res.data)
        })

        .catch(res => {
          services.errorHandling(res)
        })
    }
  }
}

export default profile
