import Vue from 'vue'
import Vuex from 'vuex'
import profile from './profile.module'
import auth from './auth.module'
import clusters from './clusters.module'
import tree from './tree.module'
import errors from './errors.module'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth,
    profile,
    clusters,
    tree,
    errors
  }
})
