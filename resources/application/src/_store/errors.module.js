import axios from 'axios'
import config from '../app.config'
import services from '../_service/services'

const errors = {
  namespaced: true,

  state: {
    errors: []
  },

  getters: {
    errors (state) {
      return state.errors
    }
  },

  mutations: {
    errors (state, payload) {
      state.errors = payload
    }
  },

  actions: {
    errors ({ state, commit }) {
      axios.get(`${config.hostApi}/ams/errors`)

        .then(res => {
          commit('errors', res.data)
        })

        .catch(res => {
          services.errorHandling(res)
        })
    }
  }
}

export default errors
