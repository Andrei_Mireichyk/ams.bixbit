import Axios from 'axios'
import config from '../app.config'
import { errorHandling } from '../_service/services'

const clusters = {
  namespaced: true,
  state: {
    clusters: {}
  },
  getters: {

    clusters (state) {
      return state.clusters
    }

  },
  mutations: {

    clusters (state, payload) {
      state.clusters = payload
    }

  },
  actions: {
    async  clusters ({ commit }) {
      await Axios.get(`${config.hostApi}/ams`)

        .then(res => {
          commit('clusters', res.data.clusters)
        })

        .catch(res => {
          errorHandling(res)
        })
    }
  }
}
export default clusters
