// Modules
import Vue from 'vue'
import services, { getHardwareError } from './_service/services'
import axios from 'axios'
import VeeValidate from 'vee-validate'
import Antd from 'ant-design-vue'
import VueApexCharts from 'vue-apexcharts'

import moment from 'moment'
import config from './app.config'
// Styles
import 'ant-design-vue/dist/antd.css'

// Configuration
Vue.config.productionTip = config.productionTip

Vue.use(VeeValidate)
Vue.use(Antd)

// Global services
Vue.prototype.$config = config
Vue.prototype.$http = axios
Vue.prototype.$moment = moment
Vue.prototype.$errorHandling = services.errorHandling
Vue.prototype.$resolveInputState = services.resolveInputState
Vue.prototype.$getHardware = services.getHardware
Vue.prototype.$getHardwareErrors = services.getHardwareErrors
Vue.prototype.$getHardwareNestedErrors = services.getHardwareNestedErrors

// Auth
const token = localStorage.getItem('token')

if (token) {
  Vue.prototype.$http.defaults.headers.common['Authorization'] = `Bearer ${token}`
}

Vue.use(VueApexCharts)

Vue.component('apexchart', VueApexCharts)
